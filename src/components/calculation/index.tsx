import React, {
    useState,
    useContext,
    useRef,
    useReducer,
    Reducer,
} from "react";

import { tryCalculate } from "./utils/calculator/calculator";
import { StatModel } from "../../types";
import Lot from "../lot";

import { FirebaseContext } from "../../context/firebase";
import { AuthUserContext } from "../../context/session";
import { LocalizationContext } from "../../context/localization";

import "../../styles/component/component.scss";
import "../../styles/toggle-button/toggle-button.scss";
import "../../styles/file-browser/file-browser.scss";
import "../../styles/calculation/calculation.scss";
import "../../styles/avatar/avatar.scss";
import "../../styles/button/button.scss";

interface CalculationProps {
    callback: (models: StatModel[], lot: string) => void;
}

const Calculation: React.FC<CalculationProps> = (props) => {
    const localization = useContext(LocalizationContext).localization;

    const fileInputRef = useRef<HTMLInputElement>(null);

    const fileReducer = (_: File[], newState: File[]) => {
        if (fileInputRef.current) fileInputRef.current.value = "";

        return newState;
    };

    const [lot, setLot] = useState<string>("");
    const [isSdMode, setSdMode] = useState<boolean>(true);
    const [files, setFiles] = useReducer<Reducer<File[], File[]>>(
        fileReducer,
        []
    );
    const [models, setModels] = useState<StatModel[]>([]);

    const firebase = useContext(FirebaseContext);
    const user = useContext(AuthUserContext);

    const onFilesChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const fileList = e.currentTarget.files as FileList;

        let fileArray: File[] = [];

        for (let i = 0; i < fileList.length; i++) fileArray.push(fileList[i]);

        const files = fileArray.filter(validFiles);

        setFiles(files);
    };

    const validFiles = (file: File) => {
        const types = /(\.xls|\.xlsx)$/i;

        return file.name.match(types);
    };

    const onCalculateClick = async () => {
        await tryCalculate(files, models, isSdMode).then((models) => {
            setModels(models);

            if (user && firebase && lot)
                firebase.setLot(user.uid, lot, {
                    models: models,
                    notes: {},
                });

            setFiles([]);

            props.callback(models, lot);
        });
    };

    const lotCallback = async (lot: string) => {
        setLot(lot);

        if (lot === "") props.callback([], "");

        if (!user || !firebase) return;

        const lotSnapshot = await firebase.getLot(user.uid, lot);

        const models = lotSnapshot.data()?.models as StatModel[];

        if (models !== undefined) {
            setModels(models);
            props.callback(models, lot);
        }
    };

    const fileSelectText =
        files.length > 1
            ? files.length + " " + localization.selected
            : files.length === 1
            ? files[0].name
            : localization.selectFiles + "...";

    return (
        <div className="component calculation">
            <div className="component__element">
                <Lot callback={lotCallback} />
            </div>

            <div className="component__element component__element_centered">
                <p className="toggle-button__text">{localization.addAverage}</p>
                <div className="toggle-button">
                    <div className="toggle-button__cover">
                        <div className="toggle-button__button">
                            <input
                                type="checkbox"
                                className="toggle-button__checkbox"
                                aria-label="Mode toggle"
                                checked={isSdMode}
                                id="mode-toggler"
                                onChange={() => setSdMode(!isSdMode)}
                            />
                            <div className="toggle-button__knobs" />
                            <div className="toggle-button__layer" />
                        </div>
                    </div>
                </div>
                <p className="toggle-button__text">
                    {localization.buildCharts}
                </p>
            </div>

            <div className="component__element">
                <p>{localization.selectFiles}:</p>

                <label className="file-browser">
                    <input
                        type="file"
                        aria-label="File browser"
                        id="calculation-file-input"
                        multiple={isSdMode}
                        ref={fileInputRef}
                        accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                        onChange={onFilesChange}
                    />
                    <span
                        className={
                            "file-browser__text " +
                            "file-browser__text_" +
                            localization.getLanguage()
                        }
                    >
                        {fileSelectText}
                    </span>
                </label>
            </div>

            <div className="component__element">
                <button className="button" style={{ backgroundColor: isSdMode ? "#0984e3" : "#00b894"}} onClick={onCalculateClick}>
                    {isSdMode
                        ? localization.buildCharts
                        : localization.addAverage}
                </button>
            </div>
        </div>
    );
};

export default Calculation;
