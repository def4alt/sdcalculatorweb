import {
	getReadModels,
	readFile,
	readSheet,
	SampleType,
	XlsxFailedToGetCellValue,
	FailedToParse,
	ReadModelBuilder,
} from "./reader";
import fs from "fs";
import { Sheet, CellObject } from "xlsx/types";
import moment from "moment";

let builder: ReadModelBuilder;

beforeEach(() => {
	builder = new ReadModelBuilder();
});

test("file type check", async () => {
	const file = new File([], "wrong_type.png");

	const readModels = await getReadModels([file]);

	expect(readModels.length).toBe(0);
});

test("reads file", async () => {
	const file = await new Promise<File>((resolve) => nodeGetFile(resolve));

	const readModels = await readFile(file);

	expect(readModels.length).toBe(20);
	expect(readModels[0].TestResults["5-Oxo Pro"]).toBe(56.362);
	expect(readModels[19].TestResults["Leu"]).toBe(401.947);
	expect(readModels[5].FailedTests.includes("Pro")).toBeTruthy();
	expect(moment(readModels[7].Date[0]).month()).toBe(8);
	expect(readModels[1].SampleType).toBe(SampleType.Lvl1);
});

const nodeGetFile = (
	resolve: (value?: File | PromiseLike<File> | undefined) => void
) => {
	fs.readFile(
		"src/components/calculation/utils/reader/test/test_file.xlsx",
		(err: NodeJS.ErrnoException | null, data: Buffer) => {
			if (err) throw err;
			const ab = new ArrayBuffer(data.length);
			let view = new Uint8Array(ab);
			for (let i = 0; i < data.length; ++i) {
				view[i] = data[i];
			}
			const file = new File([ab], "test_file.xlsx");
			resolve(file);
		}
	);
};

test("list is empty if sheet is invalid", () => {
	const sheet = {} as Sheet;
	const models = readSheet(sheet);

	expect(models.length).toBe(0);
});

test("gets model", () => {
	const sheet = {
		A4: { v: "25_09_12" } as CellObject,
		G3: { v: "test_title" } as CellObject,
		G4: { v: 20.19 } as CellObject,
		F4: { v: "failed_test" } as CellObject,
		D4: { v: "Qc lV i" } as CellObject,
		"!ref": "A1:G4",
	} as Sheet;

	builder.setRow(3);
	builder.setSheet(sheet);
	const model = builder.getModel();
	expect(model.SampleType).toBe(SampleType.Lvl1);
	expect(model.FailedTests.length).toBe(1);
	expect(moment(model.Date[0]).month()).toBe(8);
	expect(model.TestResults["test_title"]).toBe(20.19);
});

test("gets date", () => {
	const sheet = {
		A1: { v: "25_09_12" } as CellObject,
		A2: { v: "leroi 12_08_12 lorus" } as CellObject,
		A3: { v: "not a date" } as CellObject,
	} as Sheet;

	builder.setSheet(sheet);

	builder.setRow(0);
	expect(moment(builder.getDate()).date()).toEqual(25);
	builder.setRow(1);
	expect(moment(builder.getDate()).year()).toEqual(2012);
	builder.setRow(2);
	expect(moment(builder.getDate()).month()).toEqual(new Date().getMonth());
});

test("gets failed tests", () => {
	const sheet = {
		F1: { v: "failed_test" } as CellObject,
		F2: { v: "trimmed_failed_test   " } as CellObject,
	} as Sheet;

	builder.setSheet(sheet);

	builder.setRow(0);
	expect(builder.getFailedTests().length).toBe(1);
	builder.setRow(1);
	expect(builder.getFailedTests()[0]).toBe("trimmed_failed_test");
	builder.setRow(2);
	expect(builder.getFailedTests().length).toBe(0);
});

test("gets value from cell", () => {
	const sheet = {
		A1: { v: 3 } as CellObject,
		A2: { v: "text" } as CellObject,
	};

	builder.setSheet(sheet);

	expect(builder.getValueFromCell(0, 0)).toBe(3);
	expect(builder.getValueFromCell(0, 1)).toBe("text");
	expect(() => builder.getValueFromCell(0, 2)).toThrow(
		XlsxFailedToGetCellValue
	);
});

test("gets test title", () => {
	const sheet = {
		A1: { v: "title" } as CellObject,
		A2: { v: "not_trimmed_title   " } as CellObject,
	} as Sheet;

	builder.setSheet(sheet);
	expect(builder.getTestTitle(0, 0)).toBe("title");
	expect(builder.getTestTitle(0, 1)).toBe("not_trimmed_title");
	expect(builder.getTestTitle(0, 2)).toBe("");
});

test("gets test value", () => {
	const sheet = {
		A1: { v: 20.2 } as CellObject,
		A2: { v: "number" } as CellObject,
	} as Sheet;

	builder.setSheet(sheet);
	builder.setRow(0);
	expect(builder.getTestValue(0)).toBe(20.2);
	builder.setRow(1);
	expect(() => builder.getTestValue(0)).toThrow(FailedToParse);
	builder.setRow(2);
	expect(() => builder.getTestValue(0)).toThrow(FailedToParse);
});

test("gets sample type", () => {
	const sheet = {
		D1: { v: "Qc lV iI" } as CellObject,
		D2: { v: "QC LV III" } as CellObject,
		D3: { v: "Sample Type" } as CellObject,
	} as Sheet;

	builder.setSheet(sheet);
	builder.setRow(0);
	expect(builder.getSampleType()).toBe(SampleType.Lvl2);
	builder.setRow(1);
	expect(builder.getSampleType()).toBe(SampleType.Null);
	builder.setRow(2);
	expect(builder.getSampleType()).toBe(SampleType.Null);
});

test("gets test results", () => {
	const sheet = {
		G3: { v: "test_title" } as CellObject,
		G4: { v: 21.19 } as CellObject,
		H3: { v: "test_title_2" } as CellObject,
		"!ref": "A1:H4",
	} as Sheet;

	builder.setSheet(sheet);
	builder.setRow(3);
	const testResults = builder.getTestResults();
	expect(testResults["test_title"]).toBe(21.19);
	expect(testResults["test_title_2"]).toBe(undefined);
	expect(testResults).toMatchObject({ test_title: 21.19 });
});
