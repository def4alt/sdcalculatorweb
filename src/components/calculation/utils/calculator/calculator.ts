import { StatModel, getStatModels } from "../statistics/statistics";
import { getReadModels, ReadModel } from "../reader/reader";
import Westgard from "../westgard/westgard";

export const tryCalculate = (
    files: File[],
    models: StatModel[],
    isSdMode: boolean
): Promise<StatModel[]> => {
    try {
        return calculate(files, models, isSdMode);
    } catch (e) {
        console.log((e as Error).message);
        return new Promise<StatModel[]>((_) => []);
    }
};

export const calculate = async (
    files: File[],
    models: StatModel[],
    isSdMode: boolean
) => {
    const readModels = await getReadModels(files);

    validateReadModels(readModels);

    const statModels = getStatModels(readModels);

    validateStatModels(statModels);

    if (isSdMode) return statModels;
    else return appendModels(models, statModels);
};

export const validateReadModels = (models: ReadModel[]) => {
    if (models.length === 0) throw new FailedToCalculate("Failed to read file");
};

export const validateStatModels = (models: StatModel[]) => {
    if (models.length === 0)
        throw new FailedToCalculate("Failed to calculate statistics");
};

export class FailedToCalculate extends Error {}

export const appendModels = (
    oldModels: StatModel[],
    newModels: StatModel[]
): StatModel[] => {
    let models = [] as StatModel[];
    Object.assign(models, oldModels);

    newModels.forEach((newModel) => {
        const model = models.filter(
            (t) =>
                t.TestName === newModel.TestName &&
                t.SampleType === newModel.SampleType
        )[0];

        validateModel(model);

        model.Average.push(newModel.Average[0]);
        model.Date.push(newModel.Date[0]);
        model.Warnings.push(getWarning(model));
    });

    return models;
};

const validateModel = (model: StatModel) => {
    if (model === undefined)
        throw new IncompatibleStatModels(
            "Failed to compare new models and old"
        );
};

export class IncompatibleStatModels extends Error {}

export const getWarning = (model: StatModel) => {
    const westgard = new Westgard(model.SD);

    westgard.setAverageArr(model.Average);
    westgard.check();

    const warning = westgard.check();
    const previousWarning = model.Warnings[model.Warnings.length - 1];

    if (previousWarning !== warning) return warning;

    return " ";
};
