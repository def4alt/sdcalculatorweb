import React, { useState, useEffect, useContext, useCallback } from "react";
import { FiCheck, FiPlus, FiX } from "react-icons/fi";
import { FirebaseContext } from "../../context/firebase";
import { AuthUserContext } from "../../context/session";
import { LocalizationContext } from "../../context/localization";
import { User } from "firebase";

import "../../styles/lot/lot.scss";
import "../../styles/edit/edit.scss";

enum LotEditState {
    Selecting,
    Adding,
}

interface LotProps {
    callback: (lot: string) => void;
}

const Lot: React.FC<LotProps> = (props) => {
    const [lot, setLot] = useState<string>("");
    const [lotList, setLotList] = useState<string[]>([]);
    const [state, setState] = useState<LotEditState>(LotEditState.Selecting);
    const [tempLot, setTempLot] = useState<string>("");

    const firebase = useContext(FirebaseContext);
    const user = useContext(AuthUserContext);
    const localization = useContext(LocalizationContext).localization;

    const authStateChangeHandler = useCallback(
        async (user: User | null) => {
            if (!user || !firebase) {
                setLotList([]);
                selectLot("");
                return;
            }

            const docs = (await firebase.lots(user.uid).get()).docs;
            const lots = docs.map((t) => t.id).filter((t) => t !== "notes");

            setLotList(lots);
        },
        [firebase]
    );

    useEffect(() => {
        if (!firebase) return;

        const unsubscribe = firebase.auth.onAuthStateChanged(
            authStateChangeHandler
        );

        return () => unsubscribe();
    }, [firebase, authStateChangeHandler]);

    const removeLot = (index: number) => {
        if (user && firebase)
            firebase.lots(user.uid).doc(lotList[index]).delete();

        selectLot("");

        const newList = lotList.filter((_, i) => i !== index);

        setLotList(newList);
    };

    const selectLot = (lot: string) => {
        setLot(lot);
        props.callback(lot);
    };

    const addLot = (lot: string) => {
        setLotList(lotList.concat(lot));
        selectLot(lot);

        if (user && firebase && lot)
            firebase.setLot(user.uid, lot, {
                models: {},
                notes: {},
            });
    };

    const onLotInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;

        if (value === "") setTempLot("");

        const validatedValue = validateLot(value);

        if (validatedValue === "") return;

        setTempLot(validatedValue);
    };

    const onLotKeyDown = (e: React.KeyboardEvent) => {
        if (e.keyCode === 27) setState(LotEditState.Selecting);
    };

    const validateLot = (value: string) => {
        if (isNaN(Number(value))) return "";

        if (value.length > 6) return "";

        return value;
    };

    const onConfirmAddLotButtonClick = () => {
        setState(LotEditState.Selecting);

        if (tempLot !== "") addLot(tempLot);
    };

    const onAddLotButtonClick = () => {
        setTempLot("");
        setState(LotEditState.Adding);
    };

    return (
        <>
            <div className="lot__view">
                {localization.lots}
                <span className="lot__view_gray">#{lot}</span>
            </div>
            <div className="edit" onKeyDown={onLotKeyDown}>
                {lotList.map((lot, i) => (
                    <div className="edit__cell" key={i}>
                        <button
                            className="edit__select"
                            onClick={() => selectLot(lot)}
                        >
                            {lot}
                        </button>
                        <button
                            className="edit__remove"
                            onClick={() => removeLot(i)}
                        >
                            <FiX />
                        </button>
                    </div>
                ))}
                {state === LotEditState.Adding ? (
                    <div className="edit__input">
                        <input
                            type="text"
                            onChange={onLotInput}
                            value={tempLot}
                            id="lot-input"
                        />
                        <button
                            onClick={onConfirmAddLotButtonClick}
                            type="button"
                            id="confirm-add-lot-button"
                        >
                            <FiCheck />
                        </button>
                    </div>
                ) : (
                    <button
                        className="edit__add"
                        aria-label="Add lot"
                        onClick={onAddLotButtonClick}
                    >
                        <FiPlus />
                    </button>
                )}
            </div>
        </>
    );
};

export default Lot;
