const merge = require("webpack-merge");
const common = require("./webpack.common");
const DotEnv = require("dotenv-webpack");
const webpack = require("webpack");

module.exports = merge(common, {
    mode: "development",
    devServer: {
        historyApiFallback: true,
        hot: true
    },
    devtool: "cheap-module-eval-source-map",
    plugins: [
        new DotEnv({
            path: "./.env.development.local"
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.(s*)css$/,
                include: /src/,
                use: ["style-loader", "css-loader", {
                    loader: "sass-loader",
                    options: {
                        implementation: require("sass")
                    }
                }]
            }
        ]
    }
});
