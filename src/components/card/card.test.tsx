import React from "react"
import { cleanup, RenderResult, render } from "@testing-library/react"
import Card from ".";
import { StatModel, SampleType } from "../../types";

afterEach(cleanup)

let component: RenderResult;

beforeEach(() => {
    component = render(<Card model={{
        TestName: "test",
        Average: [10],
        SD: 10,
        Warnings: [" "],
        Date: [new Date().toUTCString()],
        SampleType: SampleType.Lvl1
    } as StatModel} width={300} showSDCV={true}></Card>);
})

test("renders header", () => {
    expect(component.getByText("test Lvl1")).toBeInTheDocument();
})

test("renders footer", () => {
    expect(component.getByText("SD: 10 CV: 100%")).toBeInTheDocument();
})