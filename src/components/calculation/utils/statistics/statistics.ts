import { ReadModel, SampleType } from "../reader/reader";

export type StatModel = {
	Average: number[];
	SD: number;
	TestName: string;
	SampleType: SampleType;
	Date: Array<string>;
	Warnings: Array<string>;
};

export const getStatModels = (models: ReadModel[]): StatModel[] => {
	const testnames = Object.keys(models[0].TestResults);

	let statModels: StatModel[] = [];

	testnames.forEach((testname) => {
		const model1 = tryCalculateStats(models, testname, SampleType.Lvl1);
		if (model1.Average !== undefined) statModels.push(model1);

		const model2 = tryCalculateStats(models, testname, SampleType.Lvl2);
		if (model2.Average !== undefined) statModels.push(model2);
	});

	return statModels;
};

export const tryCalculateStats = (
	models: ReadModel[],
	testname: string,
	type: SampleType
) => {
	try {
		return calculateStats(models, testname, type);
	} catch (e) {
		console.log((e as Error).message);
		return {} as StatModel;
	}
};

export const calculateStats = (
	models: ReadModel[],
	testname: string,
	type: SampleType
) => {
	const validModels = getNonFailedModels(models, testname).filter(
		(t) => t.SampleType === type
	);
    
    if (validModels.length === 0)
        throw new InvalidArgument("There is no valid models after validation")

	const testValues = validModels.map((t) => t.TestResults[testname]);

	return {
		SampleType: type,
		Average: [getAverageFor(testValues)],
		SD: getStandardDeviationOf(testValues),
		TestName: testname,
		Date: [validModels[0].Date[0]],
		Warnings: [" "],
	} as StatModel;
};

export class InvalidArgument extends Error {}

export const getNonFailedModels = (
	models: ReadModel[],
	testname: string
): ReadModel[] => {
	return models
		.filter((t) => t.FailedTests.includes(testname) === false)
		.filter((t) => testname in t.TestResults);
};

export const getAverageFor = (arr: number[]) => {
	if (arr.length === 0) return 0;

	return arr.reduce((s1, s2) => s1 + s2, 0.0) / arr.length;
};

export const getStandardDeviationOf = (arr: number[]) => {
	if (arr.length === 0) return 0;

	const average = getAverageFor(arr);

	const sqSum = arr.reduce(
		(s1, s2) => s1 + ((s2 - average) * (s2 - average)) / arr.length,
		0
	);

	return Math.sqrt(sqSum);
};
