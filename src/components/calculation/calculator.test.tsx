import React from "react";
import {
	render,
	cleanup,
	RenderResult,
	fireEvent,
} from "@testing-library/react";
import Calculator from "./index";
import { StatModel } from "./utils/statistics/statistics";

afterEach(cleanup);

let component: RenderResult;
let toggler: Element;

beforeEach(() => {
	component = render(
		<Calculator
			callback={(models: StatModel[], lot: string) => {
				return;
			}}
		></Calculator>
	);
	toggler = component.container.querySelector("#mode-toggler") as Element;
});

test("toggler default is set to sd mode (on)", () => {
	expect((toggler as HTMLInputElement).checked).toBeTruthy();
});

test("on click toggler sets to average mode (off)", () => {
	fireEvent.click(toggler);
	expect((toggler as HTMLInputElement).checked).toBeFalsy();
});

test("build charts button is renderered", () => {
	const button = component.container.querySelector(
		".button"
	) as Element;
	expect(button).toBeInTheDocument();
});

test("file input accepts only spreadsheet files (.xls, .xlsx)", () => {
	const input = component.container.querySelector(
		"#calculation-file-input"
	) as HTMLInputElement;

	const xlsxFile = new File([""], "test.xlsx", {
		type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
	});
	const xlsFile = new File([""], "test.xls", {
		type: "application/vnd.ms-excel",
	});
	const notSpreadSheetFile = new File([""], "test.txt", { type: "text/plain" });

	Object.defineProperty(input, "files", {
		value: [xlsxFile, xlsFile, notSpreadSheetFile],
	});

	fireEvent.change(input);

	expect(component.getByText("2 selected")).toBeDefined();
});
